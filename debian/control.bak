Source: openqa
Section: devel
Priority: optional
Maintainer: Philip Hands <phil@hands.com>
Uploaders: Adam Majer <adamm@debian.org>,
           Andrew Lee (李健秋) <ajqlee@debian.org>,
           Hideki Yamane <henrich@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               os-autoinst,
               systemd,
               ruby-sass,
               fdupes,
               libmojolicious-perl (>= 7.24),
### Bug#839569
#              libselenium-remote-driver-perl (>= 1.0),
### for test&runtime
#               libaliased-perl,
               libarchive-extract-perl,
               libbsd-resource-perl,
#               libcache-cache-perl,
#               libcgi-pm-perl,
               libclone-perl,
               libconfig-inifiles-perl,
#               libconfig-tiny-perl,
               libcrypt-dh-gmp-perl,
               libcss-minifier-xs-perl (>= 0.01),
               libdbd-sqlite3-perl,
               libdbix-class-perl,
               libdbix-class-deploymenthandler-perl,
               libdbix-class-dynamicdefault-perl,
               libdbix-class-optimisticlocking-perl,
               libdata-dump-perl,
               libdata-optlist-perl,
               libdatetime-perl,
               libdatetime-format-sqlite-perl,
               libdatetime-format-pg-perl,
               libcpanel-json-xs-perl,
               libtest-exception-perl,
               libpod-pom-perl,
               libmojo-sqlite-perl,
               libmojo-pg-perl,
               libmojo-ioloop-readwriteprocess-perl,
               libdbix-class-schema-config-perl,
#               libdbix-class-schema-loader-perl,
               libfile-copy-recursive-perl,
#               libio-socket-inet6-perl,
               libio-socket-ssl-perl,
               libipc-run-perl,
               libjavascript-minifier-xs-perl (>= 0.11),
               libjson-perl,
               libjson-xs-perl,
               liblist-moreutils-perl,
               libmro-compat-perl,
               libmodule-implementation-perl,
               libmodule-load-conditional-perl,
               libmodule-runtime-perl,
               libmojo-rabbitmq-client-perl,
               libmojolicious-plugin-assetpack-perl (>= 1.36),
#               libmojolicious-plugin-renderfile-perl,
               libnamespace-clean-perl,
               libnet-dbus-perl,
               libnet-openid-consumer-perl,
               libpackage-stash-perl,
               libparams-util-perl,
               libparams-validate-perl,
               libregexp-common-perl,
               libscalar-list-utils-perl,
               libsort-versions-perl,
               libsql-splitstatement-perl,
               libsql-translator-perl,
               libsub-install-perl,
               libsub-name-perl,
               libtest-compile-perl,
               libtest-simple-perl,
               libtext-markdown-perl,
               libtime-parsedate-perl,
               libtimedate-perl,
               liburi-perl,
               libwww-perl,
               perl,
               libminion-perl,
### for testcases
               libperl-critic-perl,
               libperl-critic-freenode-perl,
               libcommonmark-perl,
               libgetopt-long-descriptive-perl,
               libmojolicious-plugin-oauth2-perl,
               libtest-fatal-perl,
               libtest-mockmodule-perl,
               libtest-mockobject-perl,
               libtest-output-perl,
               libtest-pod-perl,
               libtest-strict-perl,
               libtest-warnings-perl,
               libyaml-libyaml-perl,
               libjson-validator-perl,
               libminion-backend-sqlite-perl,
               postgresql,
               rsync,
               shellcheck,
               jsbeautifier,
               libclass-c3-componentised-perl,
               libtest-most-perl,
               libyaml-pp-perl,
               python3,
               python3-future,
               python3-requests,
               yamllint,
               libfilesys-df-perl,
               libfile-map-perl,
#              for avoiding assetpack downloads
               fonts-fork-awesome,
               libjs-chosen,
               libjs-jquery-datatables,
               libjs-bootstrap4,
               libjs-bootstrap-tour,
               libjs-codemirror,
               libjs-jquery,
               libjs-jquery-timeago,
               libjs-popper.js,
               node-jquery-ujs,
Standards-Version: 4.6.0.1
Rules-Requires-Root: no
Homepage: http://open.qa
Vcs-Git: https://salsa.debian.org/debian/openqa.git
Vcs-Browser: https://salsa.debian.org/debian/openqa

Package: openqa
Architecture: all
Built-Using: node-dagre-d3-renderer (= 0.6.4+dfsg-3),
Depends: ${misc:Depends}, ${perl:Depends},
         openqa-common (>= ${binary:Version}),
         openqa-client (>= ${binary:Version}),
         sqlite3, git, optipng, dbus,
         liburi-perl,
         liblwp-protocol-https-perl,
         libdbix-class-perl (>= 0.082801),
         libsql-splitstatement-perl,
         libdbix-class-deploymenthandler-perl,
         libmojo-sqlite-perl,
         libjson-validator-perl,
         libtext-diff-perl,
         libyaml-pp-perl,
         libdatetime-format-pg-perl,
         libminion-perl,
         libmojo-pg-perl,
         libcommonmark-perl,
         postgresql,
         libclass-c3-componentised-perl,
         libfilesys-df-perl,
         fonts-fork-awesome,
         libjs-jquery-datatables,
         libjs-bootstrap-tour,
         libjs-codemirror,
         libjs-jquery,
         libjs-jquery-timeago,
         libjs-popper.js,
         node-jquery-ujs,
         libjs-cropper,
Recommends: apparmor-profiles, apparmor-utils,
            apache2, logrotate,
            libdbd-pg-perl,
            libmojolicious-plugin-oauth2-perl,
Description: automatic Operating System testing framework (webUI & scheduler)
 openQA is a testing framework
 that allows you to run tests on pretty-much anything
 that you can get 'remote' control of
 (most often, anything you can run in a VM and point VNC at).
 This allows testing of things including GUI applications,
 system boot-up (BIOS, bootloaders, kernels),
 installers and whole operating systems.
 .
 Tests (using Perl syntax) generally consist of
 little more than sequences like:
 assert_and_click 'some_icon'; assert_screen 'some_prompt'; send_key 'ret';
 which are run using the os-autoinst test engine, by a worker.
 The tags named in scripts
 can then be associated with 'needles' (elements of screenshots)
 via the webUI (either from past tests, or while controlling a live test).
 Other testing possibilities include:
 serial-connected headless systems,
 multi-host networked tests, and non-VM 'real' systems.
 .
 This package includes the job scheduler,
 the elements providing the services to allow workers to run tests and upload results,
 and the Web-based user interface.

Package: openqa-common
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libmojolicious-perl (>= 7.24),
         libjs-chosen,
### for test&runtime
         libdbd-sqlite3-perl,
         libdbix-class-perl (>= 0.082801),
         libconfig-inifiles-perl,
         libsql-translator-perl,
         libdatetime-format-sqlite-perl,
         libfile-copy-recursive-perl,
         libmojolicious-plugin-renderfile-perl,
         libmojolicious-plugin-assetpack-perl (>= 1.36),
         libaliased-perl,
         libconfig-tiny-perl,
         libdbix-class-dynamicdefault-perl,
         libdbix-class-schema-config-perl,
         libdbix-class-optimisticlocking-perl,
         libio-socket-ssl-perl,
         libdata-dump-perl,
         libtext-markdown-perl,
         libnet-dbus-perl,
         libipc-run-perl,
         libarchive-extract-perl,
         libcss-minifier-xs-perl (>= 0.01),
         libjavascript-minifier-xs-perl,
         libmojo-rabbitmq-client-perl,
         libtime-parsedate-perl,
         libsort-versions-perl,
         libbsd-resource-perl,
         libnet-openid-consumer-perl,
         libpod-pom-perl,
         libregexp-common-perl,
         libjson-validator-perl,
Recommends:
         git-lfs,
Description: automatic Operating System testing framework (common files)
 This package contains shared resources
 for openQA web-frontend and openQA workers.
 .
 openQA is a testing framework
 that allows you to run tests on pretty-much anything
 that you can 'remotely' control.
 For instance, a VM that can be accessed via VNC.

Package: openqa-client
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         libjson-perl,
         libconfig-inifiles-perl,
         libdata-dump-perl,
         libmojolicious-perl (>= 7.24),
         openqa-common (= ${source:Version}),
Suggests: python3,
          python3-future,
Description: automatic Operating System testing framework (utilities)
 This package includes scripts such as 'openqa-cli' and 'openqa-clone-job'
 to provide easy control of openQA servers, via the REST API,
 as well as commands useful when administering an OpenQA server.
 .
 openQA is a testing framework
 that allows you to run tests on pretty-much anything
 that you can 'remotely' control.
 For instance, a VM that can be accessed via VNC.


Package: openqa-worker
Architecture: all
Pre-Depends: openqa-common (>= ${binary:Version}),
Depends: ${misc:Depends},
         ${perl:Depends},
         openqa-client (>= ${binary:Version}),
         os-autoinst (>= 4.4),
         libsql-splitstatement-perl,
         libmojo-ioloop-readwriteprocess-perl,
         libnet-dns-native-perl,
         vde2,
         libfile-map-perl,
Recommends: qemu,
            libminion-backend-sqlite-perl,
Description: automatic Operating System testing framework (worker)
 openQA is a testing framework
 that allows you to run tests on pretty-much anything
 that you can get 'remote' control of
 (most often, anything you can run in a VM and point VNC at).
 This allows testing of things including GUI applications,
 system boot-up (BIOS, bootloaders, kernels),
 installers and whole operating systems.
 .
 Tests (using Perl syntax) generally consist of
 little more than sequences like:
 assert_and_click 'some_icon'; assert_screen 'some_prompt'; send_key 'ret';
 which are run using the os-autoinst test engine, by a worker.
 The tags named in scripts
 can then be associated with 'needles' (elements of screenshots)
 via the webUI (either from past tests, or while controlling a live test).
 Other testing possibilities include:
 serial-connected headless systems,
 multi-host networked tests, and non-VM 'real' systems.
 .
 This package is to be installed on a worker system (which may also be acting as
 a server). Workers register with a server (or perhaps multiple servers), and
 can then accept jobs to run, and run tests (using the os-autoinst test engine).

Package: openqa-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
Description: automatic Operating System testing framework - documentation
 Documentation for openQA (and the associated os-autoinst test engine),
 covering topics inclusing:
 installation, configuration, and basic test writing.
 .
 openQA is a testing framework
 that allows you to run tests on pretty-much anything
 that you can 'remotely' control.
 For instance, a VM that can be accessed via VNC.
