#!/usr/bin/make -f
#export DH_VERBOSE = 1

include /usr/share/dpkg/pkg-info.mk

%:
	dh $@

override_dh_auto_build:
	# forget shepherd until it is packaged:
	rm assets/stylesheets/shepherd.scss t/ui/24-feature-tour.t
	# create links to locally installed assets
	# and convince assetpack to use local copies of remote assets:
	mkdir -p assets/_debian
	ln -s /usr/share/fonts-fork-awesome assets/_debian
	ln -s /usr/share/fonts-roboto-fontface assets/_debian
	sed -i -e 's%https://cdn\.jsdelivr\.net/npm/fork-awesome@1\.2\.0/css/fork-awesome\.min\.css%_debian/fonts-fork-awesome/css/fork-awesome.css%' assets/assetpack.def
	for ext in eot svg ttf woff woff2 ; do \
		printf '\n! forkawesome-webfont.%s\n< _debian/fonts-fork-awesome/fonts/forkawesome-webfont.%s\n' $$ext $$ext >> assets/assetpack.def ; \
	done
	ln -s -s /usr/share/javascript/chosen assets/_debian/libjs-chosen
	sed -i -e 's%https://cdnjs\.cloudflare\.com/ajax/libs/chosen/1\.7\.0/%_debian/libjs-chosen/%' assets/assetpack.def
	mkdir -p assets/_debian/datatables
	cp --dereference -v -s /usr/share/javascript/jquery-datatables/css/dataTables.bootstrap4.css \
		/usr/share/javascript/jquery-datatables/jquery.dataTables.js \
		/usr/share/javascript/jquery-datatables/dataTables.bootstrap4.js assets/_debian/datatables
	sed -i -e 's%https://cdn\.datatables\.net/1\.10\.16/\(css\|js\)/%_debian/datatables/%' assets/assetpack.def
	mkdir -p assets/_debian/libjs-bootstrap4
	ln -s /usr/share/sass/bootstrap assets/_debian/libjs-bootstrap4/sass
	ln -s /usr/share/javascript/bootstrap4/js assets/_debian/libjs-bootstrap4/js
	sed -i -e 's%https://raw\.githubusercontent\.com/twbs/bootstrap/v4\.6\.1/scss/\(.*\)%_debian/libjs-bootstrap4/sass/\1%' \
	       -e 's%https://cdn\.jsdelivr\.net/npm/bootstrap@4\.6\.1/dist/js/bootstrap\.js%_debian/libjs-bootstrap4/js/bootstrap.js%' assets/assetpack.def
	sed -i -e 's%cache/raw\.githubusercontent\.com/twbs/bootstrap/v4\.6\.1/scss%_debian/libjs-bootstrap4/sass%' assets/stylesheets/openqa.scss
	ln -s /usr/share/javascript/codemirror assets/_debian/libjs-codemirror
	sed -i -e 's%https://raw\.githubusercontent\.com/codemirror/CodeMirror/8097c7e75ce7ef0512debe9967d7568626106e53/\(lib\|mode\)\(.*\)%_debian/libjs-codemirror/\1\2%' assets/assetpack.def
	ln -s /usr/share/javascript/jquery assets/_debian/libjs-jquery
	sed -i -e 's%https://code\.jquery\.com/jquery-3\.6\.0\.js%_debian/libjs-jquery/jquery.js%' assets/assetpack.def
	cp -v -r debian/missing-sources/bootstrap-4-multi-dropdown-navbar_957865f9 assets/_debian/bootstrap-4-multi-dropdown-navbar_957865f9
	sed -i -e 's%https://raw\.githubusercontent\.com/bootstrapthemesco/bootstrap-4-multi-dropdown-navbar/beta2\.0/\(css\|js\)/%_debian/bootstrap-4-multi-dropdown-navbar_957865f9/%' assets/assetpack.def
	ln -s /usr/share/javascript/jquery-timeago assets/_debian/libjs-jquery-timeago
	sed -i -e 's%http://timeago\.yarp\.com/jquery\.timeago\.js%_debian/libjs-jquery-timeago/jquery.timeago.js%' assets/assetpack.def
	ln -s /usr/share/nodejs/popper.js/dist assets/_debian/libjs-popper.js
	sed -i -e 's%https://cdnjs\.cloudflare\.com/ajax/libs/popper\.js/1\.16\.1/umd/popper\.js%_debian/libjs-popper.js/umd/popper.js%' assets/assetpack.def
	ln -s /usr/share/nodejs/jquery-ujs/src assets/_debian/node-jquery-ujs
	sed -i -e 's%https://cdnjs\.cloudflare\.com/ajax/libs/jquery-ujs/1\.2\.1/rails\.js%_debian/node-jquery-ujs/rails.js%' assets/assetpack.def
	cp -v -r debian/missing-sources/d3js_4.13.0 assets/_debian/d3js_4.13.0
	sed -i -e 's%https://d3js\.org/d3\.v4\.js%_debian/d3js_4.13.0/d3.v4.js%' assets/assetpack.def
	tar -xvz --transform='s%.*dist%assets/_debian/dagre-d3_0.5.0%' -f debian/missing-sources/dagre-d3_0.5.0/v0.5.0.tar.gz dagre-d3-0.5.0/dist/dagre-d3.js
	sed -i -e 's%https://raw\.githubusercontent\.com/dagrejs/dagre-d3/v0\.5\.0/dist/dagre-d3\.js%_debian/dagre-d3_0.5.0/dagre-d3.js%' assets/assetpack.def
	cp -v -r debian/missing-sources/anser_2.0.1 assets/_debian/anser_2.0.1
	sed -i -e 's%https://raw\.githubusercontent\.com/IonicaBizau/\(anser\)/\(2\.0\.1\)/lib/\(index\.js\)%_debian/\1_\2/\3%' assets/assetpack.def
	ln -sf /usr/share/javascript/cropper/cropper.css assets/stylesheets/cropper.css
	# ensure that there are no remote references left
	! grep http assets/assetpack.def

override_dh_auto_install:
	dh_auto_install -- SHELL="/bin/bash" GENERATE_PACKED_ASSETS_FAILS_ON_MISSING_ASSETS=1

execute_after_dh_install:
	# make #! lines Debian Perl/Python Policy compliant
	find $(CURDIR)/debian/openqa*/usr/share/openqa -type f -print0 | \
		xargs -r0 sed -i -e '1s%^\(#!/usr/bin/\)env \(perl\|python3\)%\1\2%'
	# apache vhost configs are found in .../sites-available on Debian
	sed -i 's#\(/etc/apache2/\)vhosts\.d#\1sites-available#' \
		$(CURDIR)/debian/openqa/usr/share/openqa/script/configure-web-proxy \
		$(CURDIR)/debian/openqa/etc/apache2/sites-available/openqa*.conf.template

execute_after_dh_fixperms:
	chmod -x debian/openqa/etc/logrotate.d/openqa
	chmod +x debian/openqa/usr/share/openqa/dbicdh/*/upgrade/*-*/0*-*.pl

override_dh_auto_test:
	# remeber, there used to be more disabled tests here, inspired by the RPM spec file.
	# revert the change that introduced this comment if those tests start failing again.

	# On debian we can't run test connecting to internet
	rm -f ./t/40-script_openqa-clone-custom-git-refspec.t \
		./t/40-openqa-clone-job.t
	# Not enough permission for unshare use
	rm -f ./t/32-openqa_client-script.t

	-export CI=1; export OPENQA_TEST_TIMEOUT_SCALE_CI=10 ; \
		PATH=$$PATH:$$(ls -d /usr/lib/postgresql/*/bin) FULLSTACK=1 \
		dh_auto_test -- \
		SHELL="bash -x" PROVE_ARGS='-r -v' CHECKSTYLE=0 TEST_PG_PATH=$(CURDIR)/debian/DB

execute_before_dh_auto_clean:
	-rm -rf assets/_debian/
	rm -fr t/data/openqa/testresults/ .sass-cache/ assets/cache
	rm -f assets/assetpack.db job.json t/data/openqa/pool/1/.locked t/data/openqa/pool/1/t/data/openqa/pool/1/.locked t/data/openqa/pool/1/t/data/openqa/pool/1/t/data/openqa/pool/1/.locked t/data/openqa/pool/1/t/data/openqa/pool/1/t/data/openqa/pool/1/worker-log.txt t/data/openqa/pool/1/t/data/openqa/pool/1/worker-log.txt t/data/openqa/share/factory/iso/Core-7.2.iso t/data/openqa/share/tests/opensuse/needles/inst-timezone-text.json t/data/openqa/webui/cache/asset-status.json
	rm -f t/data/openqa/share/factory/hdd/00099963-hdd_image3.qcow2 t/data/openqa/share/factory/hdd/00099963-hdd_image7.qcow2 t/data/openqa/share/factory/hdd/hdd_image2.qcow2 t/data/openqa/share/factory/hdd/hdd_image5.qcow2 t/data/openqa/share/factory/other/00099963-hdd_image3.xml t/data/openqa/share/factory/other/00099963-hdd_image4.xml
	rm -fr $(CURDIR)/debian/DB

pod2man_opts := --errors=stderr --section=1 --center "openQA Documentation" --release 'openQA $(DEB_VERSION_UPSTREAM)'
execute_before_dh_installman:
	make build-manpages -- DESTDIR=debian/tmp OPENQA_VERSION=$(DEB_VERSION_UPSTREAM)
